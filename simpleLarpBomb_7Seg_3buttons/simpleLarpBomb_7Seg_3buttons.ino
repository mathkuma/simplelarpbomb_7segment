/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <technoLARP@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Marcelin
 * ----------------------------------------------------------------------------
 */

/*
 * ----------------------------------------------------------------------------
 * This is a simple bomb/timer intended for larp (live action role playing) game 
 * and/or airsoft game. !! THIS IS NOT A REAL BOMB !! 
 * 
 * You need an arduino uno or nano board
 * a 7segment*4 display with TM1637 controller
 * a 5v piezo buzzer
 * a red LED with an appropriate resistor (330 ohms for exemple)
 * 3* momentary on/off switches, normally open
 * ----------------------------------------------------------------------------
 */
 

#include <Arduino.h>
#include <TM1637Display.h>    // library can be found here https://github.com/avishorp/TM1637

#define DEFAULTMINUTES 5
#define DEFAULTSECONDS 0

// define connection pins
// TM1637 connection pins
#define CLK_PIN 7
#define DIO_PIN 8

// various pins
#define REDLED_PIN 5
#define BUZZER_PIN 6
#define DOWNBUTTON_PIN 9
#define UPBUTTON_PIN 10
#define ACTIVATIONBUTTON_PIN 11

// define the bomb status
enum {POWERON, ACTIVATED, DETONATED};
byte bombStatus = POWERON;

// init the 4*7seg display
TM1637Display display(CLK_PIN, DIO_PIN);

// remaining time
int remainMinutes;
int remainSeconds;

// various
bool oneTimeAnimation = true;
bool statusLed;

// time reference
unsigned long int ul_PreviousMillis;
unsigned long int ul_CurrentMillis;
unsigned long int ul_Interval;



/************************************/
/*** SETUP                        ***/
/************************************/
void setup() 
{
  // configure 7seg display
  display.setBrightness(0x0f);
 
  // configure the activation button
  pinMode(ACTIVATIONBUTTON_PIN, INPUT);
  digitalWrite(ACTIVATIONBUTTON_PIN, HIGH);

  // configure the up button
  pinMode(UPBUTTON_PIN, INPUT);
  digitalWrite(UPBUTTON_PIN, HIGH);

  // configure the down button
  pinMode(DOWNBUTTON_PIN, INPUT);
  digitalWrite(DOWNBUTTON_PIN, HIGH);

  // configure the red led
  statusLed = LOW;
  pinMode(REDLED_PIN, OUTPUT);
  digitalWrite(REDLED_PIN, statusLed);

  // default remain time
  remainMinutes = DEFAULTMINUTES;
  remainSeconds = DEFAULTSECONDS;

  oneTimeAnimation = true;

  // beep one time
  shortBeep();

  ul_PreviousMillis = millis();
  ul_CurrentMillis = ul_PreviousMillis;
  ul_Interval = 200;
}
/************************************/
/*** END OF SETUP                 ***/
/************************************/




/************************************/
/*** LOOP                         ***/
/************************************/
void loop() 
{
  // loop regarding the current bomb status
  switch (bombStatus) 
  {
    case POWERON:
      // setup remain time
      powerOn();
    break;

    case ACTIVATED:
      // the bomb is activate
      activated();
    break;
    
    case DETONATED:
      // detonation animation
      detonated();
    break;
    
    default:
      // do nothing      
    break;
  }
}
/************************************/
/*** END OF LOOP                  ***/
/************************************/





/************************************/
/*** ADDITIONNAL FUNCTIONS        ***/
/************************************/

// The bomb is not yet active, waiting for time confirmation
void powerOn()
{
  // update 7seg display to display remain time
  int displayTime = remainMinutes*100 + remainSeconds;
  display.showNumberDecEx(displayTime, 0b01000000, true, 4, 0);

  // blink the red led
  ul_CurrentMillis = millis();
  if(ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
  {
    ul_PreviousMillis = ul_CurrentMillis;

    // turn on/off the red led
    statusLed = !statusLed;
    digitalWrite(REDLED_PIN, statusLed);
  }

  // check if time changed
  // UPBUTTON_PIN pressed, increase time
  if (pressedButton(UPBUTTON_PIN))
  {
    remainMinutes++;
    remainMinutes=min(60, remainMinutes);
  }

  // DOWNBUTTON_PIN pressed, decrease time
  if (pressedButton(DOWNBUTTON_PIN))
  {
    remainMinutes--;
    remainMinutes=max(0, remainMinutes);
  }

  // check if bomb has been activated
  // ACTIVATIONBUTTON_PIN pressed, activate the bomb
  if (pressedButton(ACTIVATIONBUTTON_PIN))
  {
    //bombIsActive = true;
    bombStatus = ACTIVATED;

    // bomb is now active
    // beep 2 times
    doubleBeep();
  
    // turn on red led
    digitalWrite(REDLED_PIN, HIGH);
  
    // chanbge delay to 1000 ms
    ul_Interval = 1000;
    ul_PreviousMillis = millis();
  }
}

// the bomb is active
void activated()
{  
  // check time
  if ( (remainSeconds == 0) && (remainMinutes == 0) )
  {
    // time's up !!
    bombStatus = DETONATED;
  }
  else
  {
    // some time remains
    // wait 1 s
    ul_CurrentMillis = millis();
    if(ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
    {
      ul_PreviousMillis = ul_CurrentMillis;
  
      // decrease remaining time
      remainSeconds--;
      
      // manage changing minutes
      if (remainSeconds == -1)
      {
        remainSeconds = 59;
        remainMinutes--;
      }
  
      // beep every 30 s
      if ( (remainSeconds == 0) || (remainSeconds == 30) )
      {
        shortBeep();
      }
  
      // beep every seconds in the last 10 seconds 
      if ( (remainMinutes == 0) && (remainSeconds <= 10) )
      {
        shortBeep();
      }
    
      // update 7seg display to display new time
      int displayTime = remainMinutes*100 + remainSeconds;
    
    // show the : on 7seg display
      display.showNumberDecEx(displayTime, 0b01000000, true, 4, 0);
    }
  }
}


// the bomb detonate or have detonated
void detonated()
{
  // show exploding animation one time
  if (oneTimeAnimation)
  {
    oneTimeAnimation = false;
    
    // beep 
    longBeep();

    // turn off the led led
    digitalWrite(REDLED_PIN, LOW);

    uint8_t SEG_EMPTY[] = {0x00, 0x00, 0x00, 0x00};
    uint8_t SEG_BOOM[] = {0x7C, 0x5C, 0x5C, 0x54}; // b, o, o, n
    
    for (int i=0;i<15;i++)
    {
      display.setSegments(SEG_EMPTY, 4, 0);
      delay(200);
      display.setSegments(SEG_BOOM, 4, 0);
      delay(200);
    }
  }
  else
  {
    // the bomb already exploded
    // just show a "- - - -" on display
    uint8_t SEG_EXPLODED[] = {SEG_G, SEG_G, SEG_G, SEG_G};
    display.setSegments(SEG_EXPLODED);
  }
}





// manage a momentary on/off button
bool pressedButton(int b)
{
  if (digitalRead(b)==false)
  {
    delay(50);
    while(digitalRead(b)==false)
    {
      // debounce delay
      delay(50);
    }
    return true;
  }
  else
  {
    return false;
  }
}




// various beep finction to sound the buzzer
void shortBeep() 
{
  beep(1000, 50);
}

void doubleBeep()
{
  beep(1000, 50);
  delay(200);
  beep(1000, 50);
}
 
void longBeep() 
{
  beep(1000, 5000);
}


void beep(int freqBeep, int delayBeep)
{
  tone(BUZZER_PIN, freqBeep, delayBeep);
}

/************************************/
/*** END OF ADDITIONNAL FUNCTIONS ***/
/************************************/
