# SimpleLarpBomb_7segment

A simple bomb simulator, running on arduino with a 4*7segment display & TM1637 driver   
This simple bomb/timer is intended for larp (live action role playing) game and/or airsoft game.  
!! THIS IS NOT A REAL BOMB !! 


## User manual
1. power on the bomb
1. press UP button to increase the count down timer
1. press DOWN button to decrease the count down timer
1. press ACTIVATION button to activate & start the bomb

There is no way to defuse the bomb, except by power it off

When time expire, the bomb produce a long beep noise and show a blinking "b o o n" on display . After 5 seconds, the bomb show a "- - - -". You can power it off & on to reuse it again


## Hardware
- an arduino uno or arduino nano board
- a 7segment*4 display with TM1637 controller with a : between the segments.    [This one will work fine](https://robotdyn.com/4-digit-led-display-tube-7-segments-tm1637-50x19mm-red-double-dots-clock.html)
- a 5v piezo buzzer
- a 5v red LED with an appropriate resistor (330 ohms for exemple)
- 3* momentary on/off button, normally open

## Library
You need the **tm1637.h** library to manage the 4* 7segment display. 
This library is available via the Arduino IDE library manager. you can simply use the menu:
Sketch->Include Library->Manage Libraries... Then search for tm1637 by Avishay Orpaz. 
This library is also available on [github](https://github.com/avishorp/TM1637)  



## Some exemples
<img src="./images/simpleLarpBomb_7segment_exemple1.png" width="300">  
Homemade dev board (the white JST connector is useless)  


<img src="./images/simpleLarpBomb_7segment_exemple2.png" width="300">  
arduino uno + [seedstudio tick tock shield](https://www.seeedstudio.com/Starter-Shield-EN-Tick-Tock-shield-v2-p-2768.html)  

 
## Wiring
Arduino  |  Component
---------|------------
D5 | resistor & red LED +
D6 | buzzer +
D7 | 7 segment display pin CLK
D8 | 7 segment display pin DIO
D9 | down button
D10 | up button
D11 | activation button

## Schematic
![](./images/simpleLarpBomb_7segment_schematic.png)

## Powering
There are 4 ways to powering the arduino uno board
[Some explanations here](https://www.open-electronics.org/the-power-of-arduino-this-unknown/)
1. USB plug with 5v
1. DC 2.1mm plug with 6-12v
1. 5v pin with 5v
1. Vin pin with 6-12v
